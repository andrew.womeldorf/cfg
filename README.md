# Config / Dotfiles

- [Using this repo](https://www.atlassian.com/git/tutorials/dotfiles)

Using Nix Home Manager to handle most configuration.

Also see [andrew.womeldorf/nixpkgs](https://gitlab.com/andrew.womeldorf/nixpkgs).
